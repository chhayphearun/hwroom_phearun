package com.example.phearun.hwroom_phearun.roomdb;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.migration.Migration;
import android.content.Context;
import android.support.annotation.NonNull;

import com.example.phearun.hwroom_phearun.BookEntities;

@Database(entities = {BookEntities.class},version = 1)
public abstract class MyRoomDb extends RoomDatabase {
    public abstract Bookdao bookdao();
    private static MyRoomDb INSTANCE;

    public static MyRoomDb getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (MyRoomDb.class) {
                if (INSTANCE == null) {
                    // Create database here
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            MyRoomDb.class, "books_database")
                            .allowMainThreadQueries()
                            .build();
                }
            }
        }
        return INSTANCE;
    }
    public static final Migration MIGRATION_1_2 = new Migration(1, 2) {
        @Override public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE tbl_book "+ " ADD id INT NOT NULL PRIMARY KEY AUTOINCREMENT");
        }
    };
}
