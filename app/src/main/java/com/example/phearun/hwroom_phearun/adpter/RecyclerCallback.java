package com.example.phearun.hwroom_phearun.adpter;

import android.view.View;

import com.example.phearun.hwroom_phearun.BookEntities;

public interface RecyclerCallback {
    interface onRemoveFinishListener{

    }
    interface onEditFinishListener{

    }
    interface onReadFinishListener{

    }
    void onBookOption(View v, BookEntities entities,int index);
}
