package com.example.phearun.hwroom_phearun.adpter;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.phearun.hwroom_phearun.BookEntities;
import com.example.phearun.hwroom_phearun.R;

import java.util.List;

public class BookManagmentAdapter extends RecyclerView.Adapter<BookManagmentAdapter.BookViewHolder> {
    private List<BookEntities> entities;
    private RecyclerCallback recyclerCallback;
    private Context context;
    public BookManagmentAdapter(Context context,List<BookEntities> entities){
        this.entities = entities;
        this.context = context;
        recyclerCallback = (RecyclerCallback)context;
    }
    @NonNull
    @Override
    public BookViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = View.inflate(viewGroup.getContext(),R.layout.book_list,null);
        return new BookViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BookViewHolder bookViewHolder, int i) {
        BookEntities bookEntities = entities.get(i);

        bookViewHolder.book_name.setText(bookEntities.getBook_name());
        bookViewHolder.book_price.setText(bookEntities.getBook_price());
        bookViewHolder.book_size.setText(bookEntities.getBook_size());
        bookViewHolder.book_type.setText(bookEntities.getBook_type());
        if(bookEntities.getBook_image()!=null)
        bookViewHolder.book_img.setImageURI(Uri.parse( bookEntities.getBook_image()));
    }

    @Override
    public int getItemCount() {
        return entities.size();
    }

    public class BookViewHolder extends RecyclerView.ViewHolder{
        private TextView book_name,book_price,book_type,book_size;
        private ImageView imageView,book_img;
        public BookViewHolder(@NonNull View itemView) {
            super(itemView);
            book_name = itemView.findViewById(R.id.book_name);
            book_type = itemView.findViewById(R.id.book_type);
            book_price = itemView.findViewById(R.id.book_price);
            book_size = itemView.findViewById(R.id.book_size);
            book_img = itemView.findViewById(R.id.image);
            imageView = itemView.findViewById(R.id.option);

            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                   recyclerCallback.onBookOption(view,entities.get(getAdapterPosition()),getAdapterPosition());
                }
            });
        }
    }
}
