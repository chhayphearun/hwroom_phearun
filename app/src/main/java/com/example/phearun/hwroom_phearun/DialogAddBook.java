package com.example.phearun.hwroom_phearun;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.phearun.hwroom_phearun.roomdb.Bookdao;
import com.example.phearun.hwroom_phearun.roomdb.MyRoomDb;

public class DialogAddBook extends DialogFragment {
    private Bookdao bookdao;
    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.add_book_dialog,null);
        final EditText book_name,book_price,book_size,book_type;
        ImageView imageView;
        book_size = view.findViewById(R.id.book_size);
        book_price = view.findViewById(R.id.book_price);
        book_type = view.findViewById(R.id.book_type);
        book_name = view.findViewById(R.id.book_name);
        imageView = view.findViewById(R.id.iv_image);

        MyRoomDb myRoomDb = MyRoomDb.getDatabase(getActivity());
        bookdao = myRoomDb.bookdao();
        builder.setTitle("Add new Book").setPositiveButton("Add", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                final String name = book_name.getText().toString(),
                        price = book_price.getText().toString(),
                        size = book_size.getText().toString(),
                        type = book_type.getText().toString();
                if(name.length()> 0 && price.length()> 0 && size.length()> 0 && type.length()> 0 ){
                    BookEntities bookEntities = new BookEntities(name,type,size,price,null);
                    bookdao.InsertBook(bookEntities);
                }

            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        builder.setView(view);
        return builder.create();
    }
}
