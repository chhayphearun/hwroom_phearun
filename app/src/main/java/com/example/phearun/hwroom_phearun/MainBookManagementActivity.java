package com.example.phearun.hwroom_phearun;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.phearun.hwroom_phearun.adpter.BookManagmentAdapter;
import com.example.phearun.hwroom_phearun.adpter.GridSpacingItemDecoration;
import com.example.phearun.hwroom_phearun.adpter.RecyclerCallback;
import com.example.phearun.hwroom_phearun.roomdb.Bookdao;
import com.example.phearun.hwroom_phearun.roomdb.MyRoomDb;
import com.example.phearun.hwroom_phearun.viewmodel.MyViewModel;

import java.util.ArrayList;
import java.util.List;

public class MainBookManagementActivity extends AppCompatActivity implements RecyclerCallback {
     private Bookdao bookdao;
     private RecyclerView recyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_book_management);
        recyclerView = findViewById(R.id.recyclerview);
        Uri uri = Uri.parse("android.resource://"+getPackageName()+"/drawable/book_cover");
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getApplication(),2);
        recyclerView.setLayoutManager(gridLayoutManager);
        int spanCount = 2; // 3 columns
        int spacing = 30; // 50px
        boolean includeEdge = true;
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, includeEdge));
        MyRoomDb myRoomDb = MyRoomDb.getDatabase(getApplication());
        bookdao = myRoomDb.bookdao();
        MyViewModel model = ViewModelProviders.of(this).get(MyViewModel.class);
        model.getBookEntitiesMutableLiveData().observe(this, new Observer<List<BookEntities>>() {
            @Override
            public void onChanged(@Nullable List<BookEntities> entities) {

                BookManagmentAdapter adapter = new BookManagmentAdapter(MainBookManagementActivity.this,entities);
                recyclerView.setAdapter(adapter);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void popup(View v, final BookEntities entities, final int index){
        PopupMenu popup = new PopupMenu(this,v);
        popup.getMenuInflater().inflate(R.menu.book_menu,popup.getMenu());
        popup.show();
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.edit:
                        DialogEditBook dialogEditBook = DialogEditBook.newInstance(entities,index);
                        FragmentManager manager = getSupportFragmentManager();
                        dialogEditBook.show(manager,"dialogEdit");
                        Toast.makeText(MainBookManagementActivity.this, menuItem.getTitle(), Toast.LENGTH_SHORT).show();
                        return true;
                    case R.id.remove:
                        bookdao.RemoveBook(entities);
                        Toast.makeText(MainBookManagementActivity.this, menuItem.getTitle(), Toast.LENGTH_SHORT).show();
                        return true;
                    case R.id.read:
                        Toast.makeText(MainBookManagementActivity.this, menuItem.getTitle(), Toast.LENGTH_SHORT).show();
                        return true;
                }
                return false;
            }
        });
    }

    @Override
    public void onBookOption(View v,BookEntities entities,int index) {
        popup(v,entities,index);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add_book_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.add:
                DialogAddBook dialog = new DialogAddBook();
                FragmentManager manager = getSupportFragmentManager();
                dialog.show(manager,"dialog");
                return true;
        }
        return false;
    }
}
