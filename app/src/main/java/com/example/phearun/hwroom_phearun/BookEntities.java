package com.example.phearun.hwroom_phearun;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

@Entity(tableName = "tbl_book")
public class BookEntities implements Parcelable {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int book_id;
    @ColumnInfo(name = "book_name")
    private String book_name;
    @ColumnInfo(name = "book_type")
    private String book_type;
    @ColumnInfo(name = "book_size")
    private String book_size;
    @ColumnInfo(name = "book_price")
    private String book_price;
    @ColumnInfo(name = "book_image")
    private String book_image;

    protected BookEntities(Parcel in) {
        book_id = in.readInt();
        book_name = in.readString();
        book_type = in.readString();
        book_size = in.readString();
        book_price = in.readString();
        book_image = in.readString();
    }

    public static final Creator<BookEntities> CREATOR = new Creator<BookEntities>() {
        @Override
        public BookEntities createFromParcel(Parcel in) {
            return new BookEntities(in);
        }

        @Override
        public BookEntities[] newArray(int size) {
            return new BookEntities[size];
        }
    };

    public int getBook_id() {
        return book_id;
    }

    public void setBook_id(int book_id) {
        this.book_id = book_id;
    }

    public String getBook_name() {
        return book_name;
    }

    public void setBook_name(String book_name) {
        this.book_name = book_name;
    }

    public String getBook_type() {
        return book_type;
    }

    public void setBook_type(String book_type) {
        this.book_type = book_type;
    }

    public String getBook_size() {
        return book_size;
    }

    public void setBook_size(String book_size) {
        this.book_size = book_size;
    }

    public String getBook_price() {
        return book_price;
    }

    public void setBook_price(String book_price) {
        this.book_price = book_price;
    }

    public String getBook_image() {
        return book_image;
    }

    public void setBook_image(String book_image) {
        this.book_image = book_image;
    }

    public BookEntities(String book_name, String book_type, String book_size, String book_price, String book_image) {
        this.book_name = book_name;
        this.book_type = book_type;
        this.book_size = book_size;
        this.book_price = book_price;
        this.book_image = book_image;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(book_id);
        parcel.writeString(book_name);
        parcel.writeString(book_type);
        parcel.writeString(book_size);
        parcel.writeString(book_price);
        parcel.writeString(book_image);
    }
}
