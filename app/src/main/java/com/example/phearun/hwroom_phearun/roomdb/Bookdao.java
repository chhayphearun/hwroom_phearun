package com.example.phearun.hwroom_phearun.roomdb;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.phearun.hwroom_phearun.BookEntities;

import java.util.List;

@Dao
public interface Bookdao {
    @Query("Select * from tbl_book ORDER BY id desc")
    LiveData<List<BookEntities>> getAllBooks();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void InsertBook(BookEntities...bookEntities);

    @Delete
    void RemoveBook(BookEntities...bookEntities);

    @Update
    void EditBook(BookEntities...bookEntities);

}
