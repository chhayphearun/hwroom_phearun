package com.example.phearun.hwroom_phearun;

import android.app.Dialog;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.phearun.hwroom_phearun.roomdb.Bookdao;
import com.example.phearun.hwroom_phearun.roomdb.MyRoomDb;
import com.example.phearun.hwroom_phearun.viewmodel.MyViewModel;

public class DialogEditBook extends DialogFragment {
    private Bookdao bookdao;
    private BookEntities bookEntities;
    private int index;
    public static DialogEditBook newInstance(BookEntities bookEntities,int index) {
        DialogEditBook f = new DialogEditBook();
        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putParcelable("data", bookEntities);
        args.putInt("index", index);
        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        MyRoomDb myRoomDb = MyRoomDb.getDatabase(getActivity());
        bookdao = myRoomDb.bookdao();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.add_book_dialog,null);
        final EditText book_name,book_price,book_size,book_type;
        ImageView imageView;
        book_size = view.findViewById(R.id.book_size);
        book_price = view.findViewById(R.id.book_price);
        book_type = view.findViewById(R.id.book_type);
        book_name = view.findViewById(R.id.book_name);
        imageView = view.findViewById(R.id.iv_image);
        if(getArguments()!=null){
            BookEntities bookEntities = getArguments().getParcelable("data");
            if(bookEntities!=null){
                book_name.setText(bookEntities.getBook_name());
                book_price.setText(bookEntities.getBook_price());
                book_size.setText(bookEntities.getBook_size());
                book_type.setText(bookEntities.getBook_type());
               index = bookEntities.getBook_id();
            }
        }
        builder.setTitle("Edit new Book").setPositiveButton("Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                final String name = book_name.getText().toString(),
                        price = book_price.getText().toString(),
                        size = book_size.getText().toString(),
                        type = book_type.getText().toString();
                if(name.length()> 0 && price.length()> 0 && size.length()> 0 && type.length()> 0 ){
                    bookEntities = new BookEntities(name,type,size,price,null);
                    bookEntities.setBook_id(index);
                    bookdao.EditBook(bookEntities);
                }
            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        builder.setView(view);
        return builder.create();
    }
}
