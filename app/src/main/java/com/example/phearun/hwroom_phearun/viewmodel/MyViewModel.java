package com.example.phearun.hwroom_phearun.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;

import com.example.phearun.hwroom_phearun.BookEntities;
import com.example.phearun.hwroom_phearun.roomdb.Bookdao;
import com.example.phearun.hwroom_phearun.roomdb.MyRoomDb;

import java.util.List;

public class MyViewModel extends AndroidViewModel {
    private LiveData<List<BookEntities>> bookEntitiesMutableLiveData;
    private Bookdao bookdao;
    public MyViewModel(@NonNull Application application) {
        super(application);
    }

    public LiveData<List<BookEntities>> getBookEntitiesMutableLiveData() {
        MyRoomDb myRoomDb = MyRoomDb.getDatabase(getApplication());
        bookdao = myRoomDb.bookdao();
        return bookEntitiesMutableLiveData = bookdao.getAllBooks();
    }
}
